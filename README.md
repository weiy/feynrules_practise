# This is a practise on FeynRules 

1. Official website: [https://feynrules.irmp.ucl.ac.be](https://feynrules.irmp.ucl.ac.be)

    - Please note that some instructions and tutorial files can be found in the __Attachments__ button, especially the tutorial in __2012__.

    - SMEFTatNLO: [https://feynrules.irmp.ucl.ac.be/wiki/SMEFTatNLO](https://feynrules.irmp.ucl.ac.be/wiki/SMEFTatNLO)

    <br>

1. From Lagrangian to Madgraph input files (UFO)

    - Follow the instructions in [doc/Tutorials_1209.0297.pdf](doc/Tutorials_1209.0297.pdf): page 4-12

    - check file: [software/feynrules-current/Models/SM/SM.nb](oftware/feynrules-current/Models/SM/SM.nb) to generate the Madgraph input files (UFO) step by step. 
    
        Note: 

        - Unfold each section, especially the ___Outputs and interfaces___ which generates the final UFO files

            
        <br>
        
    - The detailed output UFO can be found: [software/feynrules-current/Models/SM/Standard_Model_UFO](software/feynrules-current/Models/SM/Standard_Model_UFO) 

    - BSM UFO:

        - The BSM __.fr__ file can be found in: [software/feynrules-current/Models/MC4BSM_FeynRules](software/feynrules-current/Models/MC4BSM_FeynRules)